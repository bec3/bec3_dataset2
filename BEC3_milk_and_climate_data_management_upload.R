
###################################################

# DATA MANAGEMENT FOR BEC-3

# Author: István Fodor, WLR-ABG
# Date: 2022

# Milking and corresponding climate data are used as input
# Data cleaning, and merging milking with climate data
# .csv files are exported, these are the inputs for further analysis
# Currently the lines to export .csv files are commented out, un-comment to create these files

###################################################


setwd("C:/Users/fodor001/Documents/FodorI/BEC3/Scripts")

library(dplyr)
library(ggplot2)
library(scales)

milkdata <- read.table("C:/Users/fodor001/Documents/FodorI/BEC3/Scripts/wetransfer_farm-data_2022-05-27_1059/milking_IA.txt",
                       header = T,
                       sep = ";",
                       dec = ".")


head(milkdata)


# Data manipulation


str(milkdata)

milkdata <- milkdata %>% mutate_at(c("milking_id", "milking_oid", "farm_id", "animal_id", "lactation_id", "milking_system_id"), factor)
milkdata$started_at <- as.POSIXct(milkdata$started_at, "%Y-%m-%d %H:%M:%S", tz = "CET")
milkdata$ended_at <- as.POSIXct(milkdata$ended_at, "%Y-%m-%d %H:%M:%S", tz = "CET")
milkdata$parity_f <- factor(milkdata$parity)

milkdata$DIM_real <- with(milkdata, ifelse((as.Date(ended_at, format = "%Y-%m-%d", tz = "CET") > as.Date(started_at, format = "%Y-%m-%d", tz = "CET")) |
                                             ((as.Date(ended_at, format = "%Y-%m-%d", tz = "CET") == as.Date(started_at, format = "%Y-%m-%d", tz = "CET")) & 
                                                chron:: times(strftime(ended_at,"%H:%M:%S", tz = "CET")) - chron:: times("00:00:00") < "00:14:24"),
                                           floor(dim) + 1, ceiling(dim)))

# Assign DIM (using dim with 2 decimals as the starting point), DIM assigned based on DIM at the end of milking:
# If milking started on date 1, but ended on date 2 --> floor + 1 --> DIM of date 2
# If milking started and ended on the same day AND
# Not more than "00:14:24" (= 1/100th of a day) has elapsed at the end of milking since midnight (dim still ends with .00!) --> floor + 1 --> assign the new day
# Otherwise just return the ceiling


summary(milkdata)

milkdata_red <- milkdata %>%
  select(c("milking_id", "milking_oid", "farm_id", "animal_id", "lactation_id", "milking_system_id",
           "parity", "parity_f", "started_at", "ended_at", "mi", "DIM_real",
           "tmy", "eclf", "ecrf", "eclr", "ecrr", "eclf_lely", "ecrf_lely", "eclr_lely", "ecrr_lely",
           "speed", "dur", "milk_t", "weight"))


# Descriptive stats and plotting (raw data)


with(milkdata_red, length(milking_id))
with(milkdata_red, nlevels(milking_id)) # Duplicate milking IDs in ~1000 instances

with(milkdata_red, length(milking_oid))
with(milkdata_red, nlevels(milking_oid)) # Unique, = number of rows in the dataset

with(milkdata_red, nlevels(farm_id))
with(milkdata_red, nlevels(animal_id))
with(milkdata_red, nlevels(lactation_id))
with(milkdata_red, nlevels(milking_system_id))

with(milkdata_red, summary(parity))

ggplot(milkdata_red, aes(x = parity_f)) +
  geom_bar() +
  scale_x_discrete(breaks = seq(0, max(milkdata_red$parity), 1)) +
  scale_y_continuous(labels = comma) +
  labs(x = "Parity", y = "Number of records") +
  theme_bw() +
  theme(axis.title = element_text(size = 20),
        axis.text = element_text(size = 14)) # Number of milking records per parity


ggplot(milkdata_red, aes(x = format(as.Date(started_at, format = "%Y-%m-%d %H:%M:%S"),"%Y"))) +
  geom_bar() +
  scale_y_continuous(labels = comma) +
  labs(x = "Year", y = "Number of records") +
  theme_bw() +
  theme(axis.title = element_text(size = 20),
        axis.text = element_text(size = 14)) # Number of milking records per year

ggplot(milkdata_red, aes(x = DIM_real)) +
  geom_histogram(binwidth = 5) +
  # scale_x_discrete(breaks = seq(0, max(milkdata_red$parity), 1)) +
  scale_y_continuous(labels = comma) +
  labs(x = "Days in milk", y = "Number of records") +
  theme_bw() +
  theme(axis.title = element_text(size = 20),
        axis.text = element_text(size = 14)) # Number of milking records per DIM
# --> Discard records above 305 or 350 (avoid chance of classifying 2 lactations as 1) DIM? Especially with the bimodal distribution observed here

ggplot(milkdata_red, aes(x = tmy)) +
  geom_histogram(binwidth = 0.1) +
  scale_y_continuous(labels = comma) +
  labs(x = "Milk yield per milking", y = "Number of records") +
  theme_bw() +
  theme(axis.title = element_text(size = 20),
        axis.text = element_text(size = 14)) # Number of milking records by milk production per milking (max. does not seem to be realistic)

eclfplot <- ggplot(milkdata_red, aes(x = eclf)) +
  geom_histogram(binwidth = 0.1) +
  xlim(2, 10) +
  scale_y_continuous(limits = c(0, 2*10^5), labels = comma) +
  labs(x = "Conductivity LF", y = "Number of records") +
  theme_bw() +
  theme(axis.title = element_text(size = 20),
        axis.text = element_text(size = 14))

ecrfplot <- ggplot(milkdata_red, aes(x = ecrf)) +
  geom_histogram(binwidth = 0.1) +
  xlim(2, 10) +
  scale_y_continuous(limits = c(0, 2*10^5), labels = comma) +
  labs(x = "Conductivity RF", y = "Number of records") +
  theme_bw() +
  theme(axis.title = element_text(size = 20),
        axis.text = element_text(size = 14))

eclrplot <- ggplot(milkdata_red, aes(x = eclr)) +
  geom_histogram(binwidth = 0.1) +
  xlim(2, 10) +
  scale_y_continuous(limits = c(0, 2*10^5), labels = comma) +
  labs(x = "Conductivity LR", y = "Number of records") +
  theme_bw() +
  theme(axis.title = element_text(size = 20),
        axis.text = element_text(size = 14))

ecrrplot <- ggplot(milkdata_red, aes(x = ecrr)) +
  geom_histogram(binwidth = 0.1) +
  xlim(2, 10) +
  scale_y_continuous(limits = c(0, 2*10^5), labels = comma) +
  labs(x = "Conductivity RR", y = "Number of records") +
  theme_bw() +
  theme(axis.title = element_text(size = 20),
        axis.text = element_text(size = 14))

ecplot <- gridExtra:: grid.arrange(eclfplot, ecrfplot,
                                   eclrplot, ecrrplot,
                                   nrow = 2, ncol = 2) # Conductivity


eclf_lelyplot <- ggplot(milkdata_red, aes(x = eclf_lely)) +
  geom_histogram() +
  xlim(40, 120) +
  scale_y_continuous(limits = c(0, 2*10^5), labels = comma) +
  labs(x = "Conductivity LF", y = "Number of records") +
  theme_bw() +
  theme(axis.title = element_text(size = 20),
        axis.text = element_text(size = 14))

ecrf_lelyplot <- ggplot(milkdata_red, aes(x = ecrf_lely)) +
  geom_histogram() +
  xlim(40, 120) +
  scale_y_continuous(limits = c(0, 2*10^5), labels = comma) +
  labs(x = "Conductivity RF", y = "Number of records") +
  theme_bw() +
  theme(axis.title = element_text(size = 20),
        axis.text = element_text(size = 14))

eclr_lelyplot <- ggplot(milkdata_red, aes(x = eclr_lely)) +
  geom_histogram() +
  xlim(40, 120) +
  scale_y_continuous(limits = c(0, 2*10^5), labels = comma) +
  labs(x = "Conductivity LR", y = "Number of records") +
  theme_bw() +
  theme(axis.title = element_text(size = 20),
        axis.text = element_text(size = 14))

ecrr_lelyplot <- ggplot(milkdata_red, aes(x = ecrr_lely)) +
  geom_histogram() +
  xlim(40, 120) +
  scale_y_continuous(limits = c(0, 2*10^5), labels = comma) +
  labs(x = "Conductivity RR", y = "Number of records") +
  theme_bw() +
  theme(axis.title = element_text(size = 20),
        axis.text = element_text(size = 14))

ec_lelyplot <- gridExtra:: grid.arrange(eclf_lelyplot, ecrf_lelyplot,
                                        eclr_lelyplot, ecrr_lelyplot,
                                        nrow = 2, ncol = 2) # Conductivity according to Lely


ggplot(milkdata_red, aes(x = speed)) +
  geom_histogram(binwidth = 0.2) +
  labs(x = "Milking speed", y = "Number of records") +
  theme_bw() +
  theme(axis.title = element_text(size = 20),
        axis.text = element_text(size = 14)) # Milking speed

ggplot(milkdata_red, aes(x = mi)) +
  geom_histogram(binwidth = 1000) +
  scale_y_continuous(labels = comma) +
  labs(x = "Milking interval (sec)", y = "Number of records") +
  theme_bw() +
  theme(axis.title = element_text(size = 20),
        axis.text = element_text(size = 14))

75000/3600 # Express in hours

24*3600 # One day

ggplot(milkdata_red, aes(x = dur)) +
  geom_histogram() +
  scale_y_continuous(labels = comma) +
  labs(x = "Milking duration", y = "Number of records") +
  theme_bw() +
  theme(axis.title = element_text(size = 20),
        axis.text = element_text(size = 14)) # Milking duration

ggplot(milkdata_red, aes(x = weight)) +
  geom_histogram(binwidth = 1) +
  labs(x = "Body weight", y = "Number of records") +
  theme_bw() +
  theme(axis.title = element_text(size = 20),
        axis.text = element_text(size = 14)) # Body weight, remove if <350 kg


# More manipulation, remove unrealistic or erroneous data


dim(milkdata_red)

dim(unique(milkdata_red[, -2])) # No duplicates even without milking_oid, which uniquely identifies each row

ggplot(milkdata_red[milkdata_red$parity_f == 0, ], aes(x = tmy)) +
  geom_histogram(binwidth = 0.1) +
  scale_y_continuous(labels = comma) +
  labs(x = "Milk yield per milking", y = "Number of records") +
  theme_bw() +
  theme(axis.title = element_text(size = 20),
        axis.text = element_text(size = 14)) # Indeed, these cows (parity = 0) do have milk production (not heifer, not dry), and the milk prod. is comparable

ggplot(milkdata_red[milkdata_red$parity_f != 0, ], aes(x = tmy)) +
  geom_histogram(binwidth = 0.1) +
  scale_y_continuous(labels = comma) +
  labs(x = "Milk yield per milking", y = "Number of records") +
  theme_bw() +
  theme(axis.title = element_text(size = 20),
        axis.text = element_text(size = 14))

ggplot(milkdata_red[milkdata_red$parity_f == 0, ], aes(x = weight)) +
  geom_histogram() +
  scale_y_continuous(labels = comma) +
  labs(x = "Body weight", y = "Number of records") +
  theme_bw() +
  theme(axis.title = element_text(size = 20),
        axis.text = element_text(size = 14)) # Parity = 0 BW is OK

ggplot(milkdata_red[milkdata_red$parity_f == 0, ], aes(x = format(as.Date(started_at, format = "%Y-%m-%d %H:%M:%S"),"%Y"))) +
  geom_bar() +
  scale_y_continuous(labels = comma) +
  labs(x = "Year", y = "Number of records") +
  theme_bw() +
  theme(axis.title = element_text(size = 20),
        axis.text = element_text(size = 14)) # Something might have happened at the end of 2019, resulting in this peculiar behaviour of the data


nrow(milkdata_red[milkdata_red$parity_f == 0, ]) # ~110k such records --> exclude for now

summary(milkdata_red$ended_at) # date is OK here


milkdata_red <- milkdata_red[milkdata_red$parity_f != 0, ]

dim(milkdata_red)

summary(milkdata_red$ended_at) # date is OK here

nrow(milkdata_red[milkdata_red$DIM_real > 350, ])

allDIMnumperyear <- ggplot(milkdata_red, aes(x = format(as.Date(ended_at, format = "%Y-%m-%d %H:%M:%S"),"%Y"))) +
  geom_bar() +
  scale_y_continuous(limits = c(0, 120000), labels = comma) +
  labs(x = "Year", y = "Number of records") +
  theme_bw() +
  theme(axis.title = element_text(size = 20),
        axis.text = element_text(size = 14))

highDIMnumperyear <- ggplot(milkdata_red[milkdata_red$DIM_real > 350, ], aes(x = format(as.Date(ended_at, format = "%Y-%m-%d %H:%M:%S"),"%Y"))) +
  geom_bar() +
  scale_y_continuous(limits = c(0, 120000), labels = comma) +
  labs(x = "Year", y = "Number of records beyond 350 DIM") +
  theme_bw() +
  theme(axis.title = element_text(size = 20),
        axis.text = element_text(size = 14))

DIMnumplot <- gridExtra:: grid.arrange(allDIMnumperyear, highDIMnumperyear,
                                        nrow = 1, ncol = 2)


summary(milkdata[milkdata$ended_at >= "2020-04-26 00:00:00", ]) # Errors in raw data: dim, parity


milkdata_red <- milkdata_red[milkdata_red$DIM_real <= 350, ] # Keep only if DIM <= 350, to avoid classifying 2 lactations as 1

dim(milkdata_red)

summary(milkdata_red$tmy)

summary(milkdata_red$ended_at) # THIS IS WHERE WE LOST RECORDS FROM 2020-04-25 ONWARDS


milkdata_red$tmy <- ifelse(milkdata_red$tmy > 30, NA, milkdata_red$tmy) # Set milk prod./milking to NA if > 30 kg

summary(milkdata_red$tmy)

summary(milkdata_red$weight) # 1174 kg may be 2 cows

milkdata_red$weight <- ifelse(milkdata_red$weight > 950 | milkdata_red$weight < 350, NA, milkdata_red$weight) # Set BW to NA if < 350 or > 950 kg

summary(milkdata_red$weight)


View(milkdata[milkdata$animal_id == 289 & milkdata$parity_f == 6 & (milkdata$DIM_real == 291 | milkdata$DIM_real == 292), ])

milkdata_tobemerged <- milkdata_red %>%
  group_by(animal_id, parity_f, DIM_real) %>%
  mutate(tmy_perday = sum(tmy, na.rm = T), # MY per day (raw sum within DIM), use tally() to check No. of rows by group
         condLF_avg = mean(eclf, na.rm = T), # Mean conductivity per day per udder quarter
         condRF_avg = mean(ecrf, na.rm = T),
         condLR_avg = mean(eclr, na.rm = T),
         condRR_avg = mean(ecrr, na.rm = T),
         speed_avg = mean(speed, na.rm = T), # Mean milking speed per day
         dur_avg = mean(dur, na.rm = T), # Mean milking duration per day
         milkT_avg = mean(milk_t, na.rm = T), # Mean milk T per day
         weight_avg = mean(weight, na.rm = T), # Mean BW per day
         milkdate = max(as.Date(ended_at, tz = "CET"))) %>% # Date of milking, which is the 
  select(c("farm_id", "animal_id", "parity", "parity_f", "DIM_real", "milkdate",
           "tmy_perday", "condLF_avg", "condRF_avg", "condLR_avg", "condRR_avg",
           "speed_avg", "dur_avg", "milkT_avg", "weight_avg")) %>% # These are the variables to keep
  ungroup(DIM_real) %>% # Ungroup by DIM, so we can exclude if less than 5 DIM available (still grouped by animal and parity)
  unique() %>% # Keep unique records only (= 1 record per day)
  filter(n() >= 5) %>% # Keep only if minimum 5 days of records are available within lactation
  ungroup() # "Back to normal"


dim(milkdata_tobemerged)

milkdata_tobemerged <- milkdata_tobemerged %>%
  arrange(animal_id, parity_f, DIM_real) %>%
  group_by(animal_id, parity_f) %>%
  mutate(MY_sm = zoo:: rollmean(tmy_perday, k = 3, fill = NA), # 3-day moving average of milk yield
         weight_sm = zoo:: rollmean(weight_avg, k = 3, fill = NA), # 3-day moving average of BW
         diffDate = difftime(milkdate, dplyr:: lag(milkdate, 1)), # Diff. in consecutive dates
         diffDIM = DIM_real - dplyr:: lag(DIM_real, 1)) %>%# Diff. in consecutive DIMs
  ungroup()


str(milkdata_tobemerged)


with(milkdata_tobemerged, table(diffDate))
with(milkdata_tobemerged, table(diffDIM))

with(milkdata_tobemerged, table(diffDIM, diffDate)) # Gaps identified, up to 60 days

with(milkdata_tobemerged, sum(diag(table(diffDIM, diffDate))))/with(milkdata_tobemerged, sum(table(diffDIM, diffDate)))*100


# write.csv(milkdata_tobemerged, "milkdata_tobemerged.csv", row.names=FALSE)



######################################################################################################################


# Climate


climatedata <- read.table("C:/Users/fodor001/Documents/FodorI/BEC3/Scripts/wetransfer_farm-data_2022-05-27_1059/weather_IA.txt",
                       header = T,
                       sep = ";",
                       dec = ".")


head(climatedata)

str(climatedata)


climatedata$timestamp <- with(climatedata, lubridate:: ymd_hms(weather_id))


climatedata$THI_hourly <- with(climatedata, (1.8*temp + 32) - (0.55 - 0.0055*rel_humidity) * (1.8*temp - 26))

summary(climatedata)


with(climatedata[climatedata$datetime == "2010-01-01", ], ifelse(pracma:: trapz(x = as.numeric(factor(timestamp)), y = pmax(56, THI_hourly) - 56) >= 0,
                                                                 pracma:: trapz(x = as.numeric(factor(timestamp)), y = pmax(56, THI_hourly) - 56), 0))

with(climatedata[climatedata$datetime == "2010-01-01", ], pracma:: trapz(x = as.numeric(factor(timestamp)), y = pmax(56, THI_hourly) - 56))


climatedata <- climatedata %>%
  group_by(datetime) %>%
  mutate(THI_min = min(THI_hourly, na.rm = T),
         THI_max = max(THI_hourly, na.rm = T),
         THI_mean = mean(THI_hourly, na.rm = T),
         THI_range = max(THI_hourly, na.rm = T) - min(THI_hourly, na.rm = T),
         THI_load = pracma:: trapz(x = as.numeric(factor(timestamp)), y = pmax(56, THI_hourly, na.rm = T) - 56)) %>% # If hourly THI above 56 (results of Hut et al., 2022), contributes to load
  ungroup()


climatedata <- subset(climatedata, !is.na(THI_hourly))

summary(climatedata)

View(climatedata[climatedata$THI_load > 400, ])

climatedata_tobemerged <- climatedata %>%
  select(c(datetime, THI_min, THI_max, THI_mean, THI_range, THI_load)) %>% # Keep these
  unique()

climatedata_tobemerged$datetime <- as.Date(climatedata_tobemerged$datetime, tz = "CET") # Now CET, to fit the milking data

summary(climatedata_tobemerged)


################################################################################################################


# Merging milk production and climate data


milkdata_tobemerged <- read.table("milkdata_tobemerged.csv",
                                  header = T,
                                  sep = ",",
                                  dec = ".")

head(milkdata_tobemerged)

colnames(milkdata_tobemerged)[6] <- "datetime"

milkdata_tobemerged$datetime <- as.Date(milkdata_tobemerged$datetime, tz = "CET") # To be able to merge

dim(milkdata_tobemerged)
dim(climatedata_tobemerged)

summary(milkdata_tobemerged$datetime) # How did milkings after 2020-04-25 disappear? --> correct, dim error in raw data
summary(climatedata_tobemerged$datetime)


length(intersect(milkdata_tobemerged$datetime, climatedata_tobemerged$datetime))


milkandclimate <- merge(milkdata_tobemerged, climatedata_tobemerged, by = "datetime")

dim(milkandclimate)


summary(milkandclimate)


# write.csv(milkandclimate, "milkandclimate.csv", row.names=FALSE)











