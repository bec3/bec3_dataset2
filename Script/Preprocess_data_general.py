# -*- coding: utf-8 -*-
"""
Created on Wed Sep 21 11:02:41 2022

@author: tagha001
"""


import datetime 
import time
import cv2
import numpy as np
from os import listdir
import os
import pandas as pd 
from ast import literal_eval  
from scipy.optimize import curve_fit
from datetime import date, timedelta
from SupportFunction import daterange
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import seaborn as sns
import matplotlib.dates as md

#%%
path_to_read= r'C:\Users\tagha001\OneDrive - Wageningen University & Research\Projects\05.BEC3\Data\wetransfer_farm-data_2022-05-27_1059'
     
full_dataset= pd.read_csv(os.path.join(path_to_read,'activity_IA.csv'), sep = ';') # 

# First let's try on 2010 only as the weather information is starts from there! 
index_5y=[]
for i in range(0, full_dataset.shape[0]):
    
  # if int(full_dataset.loc[i,'datetime'].split('-')[0])>=2010 and int(full_dataset.loc[i,'datetime'].split('-')[0])<=2020:     #for weather dataset
    if int(full_dataset.loc[i,'measured_on'].split(' ')[0].split('-')[0])==2020:  # # when it time is also recored:     # for activity dataset
       
       index_5y.append(i)
   
   
dataset_5y= full_dataset.iloc[index_5y,:]        
        
dataset_5y.to_csv(os.path.join(path_to_read, 'Data_to_work', 'activity_2020.csv'), index=False, sep=';')  
#%%
path_to_read= r'C:\Users\tagha001\OneDrive - Wageningen University & Research\Projects\05.BEC3\Data\wetransfer_farm-data_2022-05-27_1059'

Weather= pd.read_csv(os.path.join(path_to_read,'Data_to_work','weather_10y.csv'), sep = ';')

date_in_range = pd.date_range(start='1/1/2010', end='12/31/2020')    #month/day/year
format= '%d-%m-%Y'
Weather_AP_day= pd.DataFrame(index=range(0,2*len(date_in_range)), columns= ['date', 'temp_AM',  'tepm_PM', 'rel_humidity_AM', 'rel_humidity_PM'])

m=0

for j in range(0, len(date_in_range)):  #
    
    date_of_I= date_in_range[j].strftime('%Y-%m-%d')

    index_date= np.where(Weather.loc[:,'datetime']== date_of_I)
    
    Weather_AP_day.loc[m,'date'] = date_of_I
    Weather_AP_day.loc[m, 'temp_AM']= np.mean(Weather.loc[index_date[0][0:11],'temp'])  # to do onyl two number after the . 
    Weather_AP_day.loc[m, 'tepm_PM']= np.mean(Weather.loc[index_date[0][12:23],'temp'])
    Weather_AP_day.loc[m, 'rel_humidity_AM']= np.mean(Weather.loc[index_date[0][0:11],'rel_humidity'])
    Weather_AP_day.loc[m, 'rel_humidity_PM']= np.mean(Weather.loc[index_date[0][12:23],'rel_humidity'])
    m=m+1
    
    
    
Weather_AP_day.to_csv(os.path.join(path_to_read, 'Data_to_work', 'weather_AP_10y.csv'), index=False, sep=';')       
#%%

#work in activity dataset! 

#work in activity dataset! 

# read the excel file     
path_to_read= r'C:\Users\tagha001\OneDrive - Wageningen University & Research\Projects\05.BEC3\Data\wetransfer_farm-data_2022-05-27_1059'

activity=  pd.read_csv(os.path.join(path_to_read,'Data_to_work','activity_2019.csv'),  sep = ';')

unique_animal_id= np.unique(activity.loc[:,'animal_id'])

# creat a new dataframe to save the results based on the AM and PM info

Activity_AP_day= pd.DataFrame(index=range(0,35*len(unique_animal_id)), columns= ['animal_id', 'date', 'activity_AM',  'activity_PM', 'parity'] )

# There are several activities reported per day per cow in the morning and in the afternoon! 
# there are 
# To test we go for 2010:
date_in_range = pd.date_range(start='1/1/2011', end='12/31/2011')    #month/day/year
format= '%Y-%m-%d'



# Sum the activity per cow two times in a day! (12AM, 12PM)

# find the unique cows number!
m=0
for i in range(0, len(unique_animal_id)):  #len(unique_animal_id)
    
   cow_id= unique_animal_id[i]
   id_index= np.where(activity.loc[:,'animal_id']== cow_id)
   new_activity= activity.iloc[id_index[0],:].reset_index()
   for j in range (0, len(date_in_range)):
       
       date_of_I= date_in_range[j].strftime('%Y-%m-%d')
       activity_day_AM=[]
       activity_day_PM=[]
     
       for k in range(0, new_activity.shape[0]):
           
           A= new_activity.loc[k,'measured_on'].split(' ')[0]
           A_date=datetime.datetime.strptime(A, format).date()
           time_of_day = new_activity.loc[k,'measured_on'].split(' ')[1]
           if new_activity.loc[k,'animal_id']== cow_id and str(A_date) == date_of_I:
               parity= new_activity.loc[k,'parity']
               
               if int(time_of_day.split(':')[0])< 12:
               # time: '00:00'
                   activity_day_AM.append(new_activity.loc[k,'activity_total'])
               else:
                   
                   activity_day_PM.append(new_activity.loc[k,'activity_total'])
                   
               
               
       total_act_AM= sum(activity_day_AM)   
       total_act_PM =  sum(activity_day_PM)  
       if total_act_AM or total_act_PM ==0:  # here is when we cannot find the date for a cow, but we need to put something for parity to overcome the error in the line        Activity_AP_day.loc[m,'parity'] = parity

            parity=-1 

       Activity_AP_day.loc[m, 'animal_id']=   cow_id
       Activity_AP_day.loc[m, 'date'] = date_of_I
       Activity_AP_day.loc[m, 'activity_AM'] = total_act_AM
       Activity_AP_day.loc[m, 'activity_PM'] = total_act_PM  
       Activity_AP_day.loc[m,'parity'] = parity
       m=m+1

Activity_AP_day.to_csv(os.path.join(path_to_read, 'Data_to_work', 'activity_AP_2011.csv'), index=False, sep=';')    

#%%
# the code from last section, it took long time to run, so we run year by year for 5 years!
path_to_read= r'C:\Users\tagha001\OneDrive - Wageningen University & Research\Projects\05.BEC3\Data\wetransfer_farm-data_2022-05-27_1059'

activity_1=  pd.read_csv(os.path.join(path_to_read,'Data_to_work','activity_AP_2010.csv'),  sep = ';')
activity_2=  pd.read_csv(os.path.join(path_to_read,'Data_to_work','activity_AP_2011.csv'),  sep = ';')
activity_3=  pd.read_csv(os.path.join(path_to_read,'Data_to_work','activity_AP_2012.csv'),  sep = ';')
activity_4=  pd.read_csv(os.path.join(path_to_read,'Data_to_work','activity_AP_2013.csv'),  sep = ';')
activity_5=  pd.read_csv(os.path.join(path_to_read,'Data_to_work','activity_AP_2014.csv'),  sep = ';')
activity_6=  pd.read_csv(os.path.join(path_to_read,'Data_to_work','activity_AP_2015.csv'),  sep = ';')
activity_7=  pd.read_csv(os.path.join(path_to_read,'Data_to_work','activity_AP_2016.csv'),  sep = ';')
activity_8=  pd.read_csv(os.path.join(path_to_read,'Data_to_work','activity_AP_2017.csv'),  sep = ';')
activity_9=  pd.read_csv(os.path.join(path_to_read,'Data_to_work','activity_AP_2018.csv'),  sep = ';')
activity_10=  pd.read_csv(os.path.join(path_to_read,'Data_to_work','activity_AP_2019.csv'),  sep = ';')
activity_11=  pd.read_csv(os.path.join(path_to_read,'Data_to_work','activity_AP_2020.csv'),  sep = ';')

Zero_activity_1= np.where(activity_1.loc[:,'activity_AM']==0) # find the zero activities indexes!
Zero_activity_2= np.where(activity_2.loc[:,'activity_AM']==0) # find the zero activities indexes!
Zero_activity_3= np.where(activity_3.loc[:,'activity_AM']==0) # find the zero activities indexes!
Zero_activity_4= np.where(activity_4.loc[:,'activity_AM']==0) # find the zero activities indexes!
Zero_activity_5= np.where(activity_5.loc[:,'activity_AM']==0) # find the zero activities indexes!
Zero_activity_6= np.where(activity_6.loc[:,'activity_AM']==0) # find the zero activities indexes!
Zero_activity_7= np.where(activity_7.loc[:,'activity_AM']==0) # find the zero activities indexes!
Zero_activity_8= np.where(activity_8.loc[:,'activity_AM']==0) # find the zero activities indexes!
Zero_activity_9= np.where(activity_9.loc[:,'activity_AM']==0) # find the zero activities indexes!
Zero_activity_10= np.where(activity_10.loc[:,'activity_AM']==0) # find the zero activities indexes!
Zero_activity_11= np.where(activity_11.loc[:,'activity_AM']==0) # find the zero activities indexes!

activity_1= activity_1.drop(Zero_activity_1[0], axis=0).reset_index().drop('index', axis=1)  
activity_2= activity_2.drop(Zero_activity_2[0], axis=0).reset_index().drop('index', axis=1)  
activity_3= activity_3.drop(Zero_activity_3[0], axis=0).reset_index().drop('index', axis=1)  
activity_4= activity_4.drop(Zero_activity_4[0], axis=0).reset_index().drop('index', axis=1)  
activity_5= activity_5.drop(Zero_activity_5[0], axis=0).reset_index().drop('index', axis=1)  
activity_6= activity_6.drop(Zero_activity_6[0], axis=0).reset_index().drop('index', axis=1)  
activity_7= activity_7.drop(Zero_activity_7[0], axis=0).reset_index().drop('index', axis=1)  
activity_8= activity_8.drop(Zero_activity_8[0], axis=0).reset_index().drop('index', axis=1)  
activity_9= activity_9.drop(Zero_activity_9[0], axis=0).reset_index().drop('index', axis=1)  
activity_10= activity_10.drop(Zero_activity_10[0], axis=0).reset_index().drop('index', axis=1)  
activity_11= activity_11.drop(Zero_activity_11[0], axis=0).reset_index().drop('index', axis=1)  

activity_AP_10y= pd.concat([activity_1,activity_2,activity_3,activity_4,activity_5,activity_6,activity_7,activity_8,activity_9,activity_10,activity_11], axis=0)

activity_AP_10y.to_csv(os.path.join(path_to_read, 'Data_to_work', 'activity_AP_10y.csv'), index=False, sep=';')    

#%%
path_to_read= r'C:\Users\tagha001\OneDrive - Wageningen University & Research\Projects\05.BEC3\Data\wetransfer_farm-data_2022-05-27_1059'
weather= pd.read_csv(os.path.join(path_to_read,'Data_to_work','weather_AP_10y.csv'),  sep = ';')
THI_data= pd.DataFrame(index=range(0,weather.shape[0]), columns= ['date','THI_AM', 'THI_PM'])
for i in range(0,weather.shape[0]):
    
    THI_data.loc[i,'THI_PM']=  (1.8* weather.loc[i,'tepm_PM']+32) - (0.55 - 0.0055 *weather.loc[i,'rel_humidity_PM'])*(1.8 *weather.loc[i,'tepm_PM'] - 26)
    THI_data.loc[i,'date'] = weather.loc[i,'date']
# calculate the THI = T - 0.55(1 - RH) (T - 58)


THI_data.to_csv(os.path.join(path_to_read, 'Data_to_work', 'THI_AP_10y.csv'), index=False, sep=';')  
#%%
# connect the weather and Activity information!
path_to_read= r'C:\Users\tagha001\OneDrive - Wageningen University & Research\Projects\05.BEC3\Data\wetransfer_farm-data_2022-05-27_1059'
activity= pd.read_csv(os.path.join(path_to_read,'Data_to_work','activity_AP_10y.csv'),  sep = ';')
weather= pd.read_csv(os.path.join(path_to_read,'Data_to_work','THI_AP_10y.csv'),  sep = ';')
weather= weather.iloc[0:4018,:]  #for 10 years
THI_data_activity= pd.DataFrame(index=range(0,activity.shape[0]), columns= ['THI_AM', 'THI_PM', 'year', 'total_THI', 'total_activity'])

for i in range(0,weather.shape[0]):
    
    DATE= weather.loc[i,'date']
    A=np.where(activity.loc[:,'date']==DATE)
    THI_data_activity.iloc[A[0][:],0:2]=weather.iloc[i,1:3]
    THI_data_activity.iloc[A[0][:],2]= DATE.split('-')[0]
    THI_data_activity.iloc[A[0][:],3]= np.round(np.mean(weather.iloc[i,1:3]),2)
    

Final_data_5y=pd.concat([activity,THI_data_activity], axis=1)
Final_data_5y['total_activity']= (Final_data_5y['activity_AM']+ Final_data_5y['activity_PM'])/2
Final_data_5y.to_csv(os.path.join(path_to_read, 'Data_to_work', 'Final_data_10y.csv'), index=False, sep=';')
#%%
#connect Istave dataset to my activity!
path_to_read= r'C:\Users\tagha001\OneDrive - Wageningen University & Research\Projects\05.BEC3\Data\wetransfer_farm-data_2022-05-27_1059'
Istvan_data= pd.read_csv(os.path.join(path_to_read,'Data_to_work' ,'milkandclimate_Marjaneh.csv'),  sep = ',')
Marji_data = pd.read_csv(os.path.join(path_to_read, 'Data_to_work','Final model','Final_data_10y.csv'), sep=';')
#select the years of 2017-2020
index_4y=[]
for i in range(0, Istvan_data.shape[0]):
    
   if int(Istvan_data.loc[i,'datetime'].split('-')[0])>=2017 and int(Istvan_data.loc[i,'datetime'].split('-')[0])<=2020:     #for weather dataset
       
       index_4y.append(i)
   
   
Istvan_data = Istvan_data.iloc[index_4y,:].reset_index(drop=True) # here the Istvan data for 4 years!
#*****
combine_data = THI_data_activity= pd.DataFrame(index=range(0,Istvan_data.shape[0]), columns= Istvan_data.columns)
act_columns = THI_data_activity= pd.DataFrame(index=range(0,Istvan_data.shape[0]), columns= ['total_activity'])
combine_data= pd.concat([combine_data, act_columns], axis=1)

for i in range(0,Istvan_data.shape[0]):
    
    DATE= Istvan_data.loc[i,'datetime']
    animal_id= Istvan_data.loc[i,'animal_id']
    A= np.where((Marji_data.loc[:,'date']==DATE) & (Marji_data.loc[:,'animal_id']==animal_id) )
    if len(A[0])!=0:
        
        combine_data.iloc[i,0:34]= Istvan_data.iloc[i,:]
        combine_data.loc[i,'total_activity'] = Marji_data.loc[A[0][0],'total_activity']

# remove the nan rows as the two dataset does not have the same records! 
combine_data=combine_data.dropna(axis=0)

combine_data.to_csv(os.path.join(path_to_read, 'Data_to_work', 'Final model' ,'Full_data_4y.csv'), index=False, sep=';')
#%%%%Plots the activityies and weather+++++++++++
# From here!! 
#%%
path_to_read= r'C:\Users\tagha001\OneDrive - Wageningen University & Research\Projects\05.BEC3\Data\wetransfer_farm-data_2022-05-27_1059'

#weather= pd.read_csv(os.path.join(path_to_read,'Data_to_work','weather_AP_10y.csv'),  sep = ';')
THI_data= pd.read_csv(os.path.join(path_to_read,'Data_to_work','THI_AP_10y.csv'),  sep = ';')
#weather= pd.concat([weather, THI_data], axis=1)
weather= THI_data.iloc[0:4018,:]  #weather.iloc[0:1817,:]
#####################################
#group the temp and humidity based on the month! 
# First make sure that the datetime column is actually of datetimes (hit it with pd.to_datetime). It's easier if it's a DatetimeIndex
weather['date']= pd.to_datetime(weather['date'])
df = weather.set_index(pd.DatetimeIndex(weather['date']))
g = df.groupby(pd.Grouper(freq="d"))
g_mean= g.mean()
g_max= g.max()
#g= pd.concat([g_mean, g_max.iloc[:,1:]], axis=1)
# plot the the AM and PM ones in the same plot! 
p=sns.scatterplot(data = g_mean.iloc[:,1]) # [2:4] for humidity
p.set_xlabel("Date", fontsize = 12)
p.set_ylabel("THI", fontsize = 12)




########################
summer_g_mean= g_mean['2010-01-01': '2020-12-31']    
# select the summer time:
ax = sns.lineplot(data= summer_g_mean.iloc[:,:])  # change the columns number based on what we used (weather, weather+THI)

#ax.errorbar(summer_g_mean.index, summer_g_mean.iloc[:,4], yerr=summer_g_st.iloc[:,4], fmt='-o') #fmt=None to plot bars only
ax.set_ylabel("THI")
#ax.xaxis.set_major_locator(md.WeekdayLocator(byweekday = 1))
ax.xaxis.set_major_locator(md.WeekdayLocator(byweekday = 48))
plt.setp(ax.xaxis.get_majorticklabels(), rotation = 90)

plt.show()


#%%

path_to_read= r'C:\Users\tagha001\OneDrive - Wageningen University & Research\Projects\05.BEC3\Data\wetransfer_farm-data_2022-05-27_1059'
activity=  pd.read_csv(os.path.join(path_to_read,'Data_to_work','activity_AP_10y.csv'),  sep = ';')
activity=activity.iloc[:,1:4]
activity=activity.dropna(axis=0)
activity['date']= pd.to_datetime(activity['date'])
df = activity.set_index(pd.DatetimeIndex(activity['date']))

g= df.groupby(pd.Grouper(freq="m"))

g_mean= g.mean()
g_st= g.std()
# select the summer time:

#plot the activity per day mean and std!

#summer_g_mean= g_mean['2019-06-01': '2019-09-30']    
# select the summer time:
#summer_g_st= g_st['2019-06-01': '2019-09-30']
  
ax = sns.lineplot(data= g_mean.iloc[:,1]) #, err_style="unit_traces")

ax.errorbar(g_mean.index, g_mean.iloc[:,1], yerr=g_st.iloc[:,1], fmt='-o') #fmt=None to plot bars only
ax.set_ylabel("Activity_PM", fontsize = 10)
#ax.xaxis.set_major_locator(md.WeekdayLocator(byweekday = 1))
plt.setp(ax.xaxis.get_majorticklabels(), rotation = 90)

plt.show()
#%%
# SubPlots the plots of THI, activity AM and activity PM for 2019
path_to_read= r'C:\Users\tagha001\OneDrive - Wageningen University & Research\Projects\05.BEC3\Data\wetransfer_farm-data_2022-05-27_1059'
weather= pd.read_csv(os.path.join(path_to_read,'Data_to_work','weather_AP_5y.csv'),  sep = ';')
THI_data= pd.read_csv(os.path.join(path_to_read,'Data_to_work','THI_AP_5y.csv'),  sep = ';')
weather= pd.concat([weather, THI_data], axis=1)
weather= weather.iloc[730:1094,:]  # to have weather 2019
#####################################
#group the temp and humidity based on the month! 
# First make sure that the datetime column is actually of datetimes (hit it with pd.to_datetime). It's easier if it's a DatetimeIndex
weather['date']= pd.to_datetime(weather['date'])
df = weather.set_index(pd.DatetimeIndex(weather['date']))
g = df.groupby(pd.Grouper(freq="d"))
g_mean= g.mean()
summer_g_mean_w= g_mean['2019-06-01': '2019-09-30']    



activity=activity_3
activity['date']= pd.to_datetime(activity['date'])
df = activity.set_index(pd.DatetimeIndex(activity['date']))
g= df.groupby(pd.Grouper(freq="d"))
g_mean= g.mean()
g_st= g.std()
# select the summer time:
#plot the activity per day mean and std!
summer_g_mean_A= g_mean['2019-06-01': '2019-09-30']    
# select the summer time:
summer_g_st_A= g_st['2019-06-01': '2019-09-30']  


fig, axes = plt.subplots(3,1,figsize=(10, 15))
fig.suptitle('Summer time in 2019', fontsize=22,fontweight="bold")


sns.lineplot(ax= axes[0],data= summer_g_mean_w.iloc[:,4:])  # change the columns number based on what we used (weather, weather+THI)

sns.lineplot(ax= axes[1],data= summer_g_mean_A.iloc[:,1]).errorbar(summer_g_mean_A.index, summer_g_mean_A.iloc[:,1], yerr=summer_g_st_A.iloc[:,1], fmt='-o') #fmt=None to plot bars only

sns.lineplot(ax= axes[2],data= summer_g_mean_A.iloc[:,2]).errorbar(summer_g_mean_A.index, summer_g_mean_A.iloc[:,2], yerr=summer_g_st_A.iloc[:,2], fmt='-o') #fmt=None to plot bars only

plt.setp(axes[0].set(ylabel= 'THI'), fontsize=18)

plt.setp(axes[1].set(ylabel= 'Activity_AM'), fontsize=18)

plt.setp(axes[2].set(ylabel= 'Activity_PM'), fontsize=18)

plt.setp(axes[0].get_legend().get_texts(), fontsize=15) # for legend text

axes[2].xaxis.set_major_locator(md.WeekdayLocator(byweekday = 1))

plt.setp(axes[2].xaxis.get_majorticklabels(), rotation = 90, fontsize=18)


for ax in fig.get_axes():
    ax.label_outer()

plt.show()

#%%








