# -*- coding: utf-8 -*-
"""
Created on Wed Jun 15 14:35:13 2022

@author: tagha001
"""

#Add libraries


import datetime 
import time
import cv2
import numpy as np
from os import listdir
import os
import pandas as pd 
from ast import literal_eval  
from scipy.optimize import curve_fit
from datetime import date, timedelta
from SupportFunction import daterange
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import seaborn as sns

#%%
#Convert txt file (data stored in txt format) into csv files
path_to_read= r'C:\Users\tagha001\OneDrive - Wageningen University & Research\Projects\05.BEC3\Data\wetransfer_farm-data_2022-05-27_1059'
dataframe1 = pd.read_csv(os.path.join(path_to_read, "activity_IA.txt"),delimiter = ';')
dataframe1.to_csv(os.path.join(path_to_read,'activity_IA.csv'), index = None, sep = ';')
#%%
# match the important features across different csv file with the date! 

#Milking= pd.read_csv(os.path.join(path_to_read,'milking_IA.csv'), sep = ';')
Activity=  pd.read_csv(os.path.join(path_to_read,'activity_IA.csv'), sep = ';')

dhi = pd.read_csv(os.path.join(path_to_read,'dhi_IA.csv'), sep = ';')
Weather= pd.read_csv(os.path.join(path_to_read,'weather_IA.csv'), sep = ';')
Lactation= pd.read_csv(os.path.join(path_to_read,'lactation_IA.csv'), sep = ';')
       
#%%            
# Working with dataset only on 2020 for a test!
path_to_read= r'C:\Users\tagha001\OneDrive - Wageningen University & Research\Projects\05.BEC3\Data\wetransfer_farm-data_2022-05-27_1059'
     
full_dataset= pd.read_csv(os.path.join(path_to_read,'activity_IA.csv'), sep = ';')

# First let's try on 2010 only as the weather information is starts from there! 
index_2020=[]
for i in range(0, full_dataset.shape[0]):
    
  # if full_dataset.loc[i,'datetime'].split('-')[0]=='2020':    # when it time is also recored: #full_dataset.loc[i,'datetime'].split(' ')[0].split('-')[0]=='2020':
   if full_dataset.loc[i,'measured_on'].split(' ')[0].split('-')[0]=='2021':    
       index_2020.append(i)
   
   
dataset_2020= full_dataset.iloc[index_2020,:]        
        
dataset_2020.to_csv(os.path.join(path_to_read, 'Data_to_work', 'activity_2021.csv'), index=False, sep=';')            
             
       
#%%
# make a weather info into AM and PM

path_to_read= r'C:\Users\tagha001\OneDrive - Wageningen University & Research\Projects\05.BEC3\Data\wetransfer_farm-data_2022-05-27_1059'

Weather= pd.read_csv(os.path.join(path_to_read,'Data_to_work','weather_2020.csv'), sep = ';')

date_in_range = pd.date_range(start='1/1/2020', end='12/31/2020')    #month/day/year
format= '%d-%m-%Y'
Weather_AP_day= pd.DataFrame(index=range(0,2*len(date_in_range)), columns= ['date', 'temp_AM',  'tepm_PM', 'rel_humidity_AM', 'rel_humidity_PM'])

m=0

for j in range(0, len(date_in_range)):  #
    
    date_of_I= date_in_range[j].strftime('%Y-%m-%d')

    index_date= np.where(Weather.loc[:,'datetime']== date_of_I)
    
    Weather_AP_day.loc[m,'date'] = date_of_I
    Weather_AP_day.loc[m, 'temp_AM']= np.mean(Weather.loc[index_date[0][0:11],'temp'])  # to do onyl two number after the . 
    Weather_AP_day.loc[m, 'tepm_PM']= np.mean(Weather.loc[index_date[0][12:23],'temp'])
    Weather_AP_day.loc[m, 'rel_humidity_AM']= np.mean(Weather.loc[index_date[0][0:11],'rel_humidity'])
    Weather_AP_day.loc[m, 'rel_humidity_PM']= np.mean(Weather.loc[index_date[0][12:23],'rel_humidity'])
    m=m+1
    
    
    
Weather_AP_day.to_csv(os.path.join(path_to_read, 'Data_to_work', 'weather_AP_2020.csv'), index=False, sep=';')             # to do, it saved wrongly!!!
#%%
path_to_read= r'C:\Users\tagha001\OneDrive - Wageningen University & Research\Projects\05.BEC3\Data\wetransfer_farm-data_2022-05-27_1059'
weather= pd.read_csv(os.path.join(path_to_read,'Data_to_work','weather_AP_5y.csv'),  sep = ';')
THI_data= pd.DataFrame(index=range(0,weather.shape[0]), columns= ['THI_AM', 'THI_PM'])
for i in range(0,weather.shape[0]):
    
    THI_data.loc[i,'THI_AM']=  (1.8* weather.loc[i,'temp_AM']+32) - (0.55 - 0.0055 *weather.loc[i,'rel_humidity_AM'])*(1.8 *weather.loc[i,'temp_AM'] - 26)
    THI_data.loc[i,'THI_PM']=  (1.8* weather.loc[i,'tepm_PM']+32) - (0.55 - 0.0055 *weather.loc[i,'rel_humidity_PM'])*(1.8 *weather.loc[i,'tepm_PM'] - 26)

# calculate the THI = T - 0.55(1 - RH) (T - 58)


THI_data.to_csv(os.path.join(path_to_read, 'Data_to_work', 'THI_AP_5y.csv'), index=False, sep=';')             # to do, it saved wrongly!!!

#%%
#work in activity dataset! 

# read the excel file     
path_to_read= r'C:\Users\tagha001\OneDrive - Wageningen University & Research\Projects\05.BEC3\Data\wetransfer_farm-data_2022-05-27_1059'

activity=  pd.read_csv(os.path.join(path_to_read,'Data_to_work','activity_2020.csv'),  sep = ';')

unique_animal_id= np.unique(activity.loc[:,'animal_id'])

# creat a new dataframe to save the results based on the AM and PM info

Activity_AP_day= pd.DataFrame(index=range(0,30*len(unique_animal_id)), columns= ['animal_id', 'date', 'activity_AM',  'activity_PM', 'parity'] )

# There are several activities reported per day per cow in the morning and in the afternoon! 
# there are 
# To test we go for 2010:
date_in_range = pd.date_range(start='1/1/2020', end='12/31/2020')    #month/day/year
format= '%d-%m-%Y'



# Sum the activity per cow two times in a day! (12AM, 12PM)

# find the unique cows number!
m=0
for i in range(0, 10):  #len(unique_animal_id)
    
   cow_id= unique_animal_id[i]
   id_index= np.where(activity.loc[:,'animal_id']== cow_id)
   new_activity= activity.iloc[id_index[0],:].reset_index()
   for j in range (0, len(date_in_range)):
       
       date_of_I= date_in_range[j].strftime('%Y-%m-%d')
       activity_day_AM=[]
       activity_day_PM=[]
     
       for k in range(0, new_activity.shape[0]):
           
           A= new_activity.loc[k,'measured_on'].split(' ')[0]
           A_date=datetime.datetime.strptime(A, format).date()
           time_of_day = new_activity.loc[k,'measured_on'].split(' ')[1]
           if new_activity.loc[k,'animal_id']== cow_id and str(A_date) == date_of_I:
               parity= new_activity.loc[k,'parity']
               
               if int(time_of_day.split(':')[0])< 12:
               # time: '00:00'
                   activity_day_AM.append(new_activity.loc[k,'activity_total'])
               else:
                   
                   activity_day_PM.append(new_activity.loc[k,'activity_total'])
                   
               
               
       total_act_AM= sum(activity_day_AM)   
       total_act_PM =  sum(activity_day_PM)  
        

       Activity_AP_day.loc[m, 'animal_id']=   cow_id
       Activity_AP_day.loc[m, 'date'] = date_of_I
       Activity_AP_day.loc[m, 'activity_AM'] = total_act_AM
       Activity_AP_day.loc[m, 'activity_PM'] = total_act_PM
       Activity_AP_day.loc[m,'parity'] = parity
       m=m+1

Activity_AP_day.to_csv(os.path.join(path_to_read, 'Data_to_work', 'activity_AP_10_cows_2020.csv'), index=False, sep=';')           
#%%

path_to_read= r'C:\Users\tagha001\OneDrive - Wageningen University & Research\Projects\05.BEC3\Data\wetransfer_farm-data_2022-05-27_1059'

activity=  pd.read_csv(os.path.join(path_to_read,'Data_to_work','activity_AP_2020.csv'),  sep = ';')
unique_animal_id= np.unique(activity.loc[:,'animal_id'])

# drop the cows that killed in that year!

zero_activity= np.where(activity.loc[:,'date_PM']==0) # find the zero activities indexes!
## here is the code if the acitivty of cows is zero for a reason, remove the cow from the list!!
#animal_dead_cows_id= np.unique(activity.loc[dead_cows[0],'animal_id']) # find the unique cow id of dead cows
#removed_index=[]
#for i in range(0,len(animal_dead_cows_id)):
    
 #   A= np.where(activity.loc[:,'animal_id']==animal_dead_cows_id[i])[0]
  #  removed_index.append(A) # it creats a list of array!!

#B=np.concatenate( removed_index, axis=0 ) # covnert the list of array into one array! 


# To have only alive cow in the dataset! 
new_activity= activity.drop(zero_activity[0], axis=0).reset_index().drop('index', axis=1)  
new_activity.to_csv(os.path.join(path_to_read, 'Data_to_work', 'activity_AP_non_zero_2020.csv'), index=False, sep=';')           

#%%
# plots of weather info
path_to_read= r'C:\Users\tagha001\OneDrive - Wageningen University & Research\Projects\05.BEC3\Data\wetransfer_farm-data_2022-05-27_1059'
weather= pd.read_csv(os.path.join(path_to_read,'Data_to_work','weather_AP_2020.csv'),  sep = ';')
THI_data= pd.read_csv(os.path.join(path_to_read,'Data_to_work','THI_AP_2020.csv'),  sep = ';')
weather= pd.concat([weather, THI_data], axis=1)
weather= weather.iloc[0:366,:]
#group the temp and humidity based on the month! 
# First make sure that the datetime column is actually of datetimes (hit it with pd.to_datetime). It's easier if it's a DatetimeIndex
weather['date']= pd.to_datetime(weather['date'])
df = weather.set_index(pd.DatetimeIndex(weather['date']))
g = df.groupby(pd.Grouper(freq="d"))

g_mean= g.mean()
g_st= g.std()

#g= pd.concat([g_mean, g_max.iloc[:,1:]], axis=1)
# plot the the AM and PM ones in the same plot! 
#p=sns.scatterplot(data = g_mean.iloc[:,4:]) # [2:4] for humidity
#p.set_xlabel("Date", fontsize = 12)
#p.set_ylabel("THI", fontsize = 12)



#Plot summer time! 
summer_g_mean= g_mean['2020-06-01': '2020-09-30']    
# select the summer time:
ax = sns.lineplot(data= summer_g_mean.iloc[:,4:])  # change the columns number based on what we used (weather, weather+THI)

#ax.errorbar(summer_g_mean.index, summer_g_mean.iloc[:,4], yerr=summer_g_st.iloc[:,4], fmt='-o') #fmt=None to plot bars only
ax.set_ylabel("THI")
ax.xaxis.set_major_locator(md.WeekdayLocator(byweekday = 1))

plt.setp(ax.xaxis.get_majorticklabels(), rotation = 90)

plt.show()


#%%
path_to_read= r'C:\Users\tagha001\OneDrive - Wageningen University & Research\Projects\05.BEC3\Data\wetransfer_farm-data_2022-05-27_1059'
activity=  pd.read_csv(os.path.join(path_to_read,'Data_to_work','activity_AP_non_zero_2020.csv'),  sep = ';')
uniq_id= np.unique(activity.loc[:,'animal_id']) 
# get the acitivty per months of individual cows and all cows! 
activity['date']= pd.to_datetime(activity['date'])
df = activity.set_index(pd.DatetimeIndex(activity['date']))

g= df.groupby(pd.Grouper(freq="d"))

g_mean= g.mean()
g_st= g.std()
 
#plot the AM PM 
p=sns.lineplot(data = g_mean.iloc[:,1:3], ci = g_st.iloc[:,1:3])
p.set_xlabel("Date", fontsize = 12)
p.set_ylabel("Activity", fontsize = 12)



#%%#
#Plot_THI
path_to_read= r'C:\Users\tagha001\OneDrive - Wageningen University & Research\Projects\05.BEC3\Data\wetransfer_farm-data_2022-05-27_1059'
activity=  pd.read_csv(os.path.join(path_to_read,'Data_to_work','activity_AP_non_zero_2020.csv'),  sep = ';')
activity['date']= pd.to_datetime(activity['date'])
df = activity.set_index(pd.DatetimeIndex(activity['date']))

g= df.groupby(pd.Grouper(freq="d"))

g_mean= g.mean()
g_st= g.std()
# select the summer time:
summer_g_mean= g_mean['2020-06-01': '2020-08-31']  
summer_g_st= g_st['2020-06-01': '2020-08-31']  
  
#p=sns.scatterplot(data = g_mean.iloc[:,2:4])
#p.set_xlabel("Date", fontsize = 12)
#p.set_ylabel("Activity", fontsize = 12)
ax = sns.lineplot(data= summer_g_mean.iloc[:,2]) #, err_style="unit_traces")

ax.errorbar(g_mean.index, g_mean.iloc[:,2], yerr=g_st.iloc[:,2], fmt='-o') #fmt=None to plot bars only

plt.show()

#%%
#plot the activity per day mean and std!
import matplotlib.dates as md

summer_g_mean= g_mean['2020-06-01': '2020-09-30']    
# select the summer time:
summer_g_st= g_st['2020-06-01': '2020-09-30']  
ax = sns.lineplot(data= summer_g_mean.iloc[:,1]) #, err_style="unit_traces")

ax.errorbar(summer_g_mean.index, summer_g_mean.iloc[:,1], yerr=summer_g_st.iloc[:,1], fmt='-o') #fmt=None to plot bars only
ax.set_ylabel("Activity_AM", fontsize = 10)
ax.xaxis.set_major_locator(md.WeekdayLocator(byweekday = 1))
plt.setp(ax.xaxis.get_majorticklabels(), rotation = 90)

plt.show()



#%%

#plot the daily info but less crowded numbers in the date axis!!

















