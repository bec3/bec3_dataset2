# -*- coding: utf-8 -*-
"""
Created on Mon Dec 19 10:10:38 2022

@author: tagha001
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Aug 26 16:03:17 2022

@author: tagha001
"""

#add Libeary 
import datetime 
import time
import cv2
import numpy as np
from os import listdir
import os
import pandas as pd
from ast import literal_eval  
from scipy.optimize import curve_fit
import statsmodels.api as sm
import statsmodels.formula.api as smf
import matplotlib.pyplot as plt
from statsmodels.graphics.api import abline_plot
import scipy.stats as stats
import seaborn as sns
import warnings
from statsmodels.tools.sm_exceptions import ConvergenceWarning
warnings.simplefilter('ignore', ConvergenceWarning)
import matplotlib.dates as md

#%%
path_to_read= r'C:\Users\tagha001\OneDrive - Wageningen University & Research\Projects\05.BEC3\Data\wetransfer_farm-data_2022-05-27_1059'
data_10y =  pd.read_csv(os.path.join(path_to_read,'Data_to_work','Final model','combine_data_10y.csv'),  sep = ';')
index_4y=[]
for i in range(0, data_10y.shape[0]):
    
   if int(data_10y.loc[i,'datetime'].split('-')[0])>=2017 and int(data_10y.loc[i,'datetime'].split('-')[0])<=2020:     #for weather dataset
       
       index_4y.append(i)
   
   
data_4y = data_10y.iloc[index_4y,:].reset_index(drop=True) # here the Istvan data for 4 years!
data_4y=data_4y.dropna(axis=0)
data_4y.to_csv(os.path.join(path_to_read, 'Data_to_work', 'Final model' ,'combine_data_4y.csv'), index=False, sep=';')


#%% #Ines you can start from here:
    
path_to_read= r'C:\Users\tagha001\OneDrive - Wageningen University & Research\Projects\05.BEC3\Data\wetransfer_farm-data_2022-05-27_1059'

data =  pd.read_csv(os.path.join(path_to_read,'Data_to_work','Final model','combine_data_4y.csv'),  sep = ';')


# in Data_5y, there is total activity and total THI (mean) per day! 
#to check if activity is normal distribution or not!
n, bins, patches = plt.hist(data.loc[:,'total_activity'], 100, density=False, facecolor='g', alpha=0.5)  
plt.ylabel('count')
plt.xlabel('Total_activity')
plt.title('Total_activity')
plt.show()   
#%%
# remove outliers
A_outliers= np.where((data.loc[:,'total_activity']>=600) | (data.loc[:,'total_activity']<= 100) )     
data=data.drop(A_outliers[0],axis=0).reset_index().drop('index', axis=1)  
#check again the acitivity distribution!
n, bins, patches = plt.hist(data.loc[:,'total_activity'], 100, density=False, facecolor='g', alpha=0.5)  
plt.ylabel('count')
plt.xlabel('Total_activity')
plt.title('Remove_outliers_total_activity')
plt.show()   

#%%
#prepare a dataset for input model!

# #do the log transformation on the response variable
data_input= data[['animal_id','DIM_cat', 'THI_mean_56', 'parity_group','year']]
log_activity= np.log(data['total_activity']).rename('log_activity')

data_input= pd.concat([log_activity, data_input], axis=1)


data_input['parity_group']= data_input['parity_group'].astype('category')
#data_input['THI_mean_cat']= data_input['THI_mean_cat'].astype('category')  # check if you use thi_mean_56 then it is not in 
data_input['DIM_cat']= data_input['DIM_cat'].astype('category')
data_input['year']= data_input['year'].astype('category')

# plot log_activity:
    
n, bins, patches = plt.hist(data_input.loc[:,'log_activity'], 100, density=False, facecolor='g', alpha=0.5)
plt.ylabel('count')
plt.xlabel('Total_activity')
plt.title('Log_activity')
plt.show()   

#%%                
            # change the log_activity or total_activity
model = smf.mixedlm("log_activity ~ DIM_cat+THI_mean_56+parity_group+year++DIM_cat*parity_group+DIM_cat* THI_mean_56+parity_group*THI_mean_56", data_input, groups = data["animal_id"], re_formula="~THI_mean_56") #, re_formula="~THI_mean_56"
results = model.fit(method = 'lbfgs') #method = 'lbfgs', 'bfgs', 'cg'               #
#mixed_fit.random_effects
#print the summary
print(results.summary())

# %%
#Convert back from the log_activity
actcual= data['total_activity']
estimated= np.exp(results.fittedvalues)
res= actcual - estimated

plt.scatter(estimated, res, alpha = 0.5)  #
plt.title("Residual vs. Fitted")
plt.xlabel("Fitted Values")
plt.ylabel("Residuals")
plt.show()
#%%plt.savefig('python_plot.png',dpi=300)
actcual= data['total_activity']
estimated= np.exp(results.fittedvalues)
res= actcual - estimated
fig = sm.qqplot(res)
plt.show()


fig = sm.qqplot(res, stats.t, fit=True, line="45") 
plt.show()

#%%Plot the histogram Distribution of residuals
actcual= data['total_activity']
estimated= np.exp(results.fittedvalues)
res= actcual - estimated
n, bins, patches = plt.hist(res, 100, density=False, facecolor='g', alpha=0.5)
plt.ylabel('count')
plt.xlabel('Residuals')
plt.show() 
#%%
#plot random_intercept vs Random_slop (THI)
B= results.random_effects
df_B= pd.DataFrame.from_dict(B,orient='index')

_ = sns.lmplot(x="Group", y="THI_mean_56", data=df_B)


#%%
#plot predicted activity values vs THI_mean_56
y_hat= np.exp(results.fittedvalues)
y_hat= pd.Series.to_frame(y_hat, name='predicted_total_activity')

data_plot= pd.concat([y_hat,data_input.loc[:,'THI_mean_56'], data_input.loc[:,'animal_id']],axis=1)
_ = sns.lmplot(x="THI_mean_56", y="predicted_total_activity", data=data_plot, hue= 'animal_id', scatter= True, legend=False,x_ci= 'sd')

#lgend= facet_kws=dict(sharex=False, sharey=False)
#%%
#plot the THI effect by parity and lactation stage for predicted activity
y_hat= np.exp(results.fittedvalues)
y_hat= pd.Series.to_frame(y_hat, name='predicted_total_activity')

data_plot= pd.concat([y_hat,data_input.loc[:,'THI_mean_56'], data_input.loc[:,'parity_group'], data_input.loc[:,'DIM_cat']],axis=1)

_ = sns.lmplot(x="THI_mean_56", y="predicted_total_activity", data=data_plot, hue= 'parity_group', col='DIM_cat',scatter= False,x_ci= 'sd')

#%%

#plot the THI effect by parity and lactation stage for oriignal activity
#y_hat= results.fittedvalues
#y_hat= pd.Series.to_frame(y_hat, name='original_total_activity')

data_plot= pd.concat([data_input.loc[:,'total_activity'],data_input.loc[:,'THI_mean_56'], data_input.loc[:,'parity_group'], data_input.loc[:,'DIM_cat']],axis=1)

_ = sns.lmplot(x="THI_mean_56", y="total_activity", data=data_plot, hue= 'parity_group', col='DIM_cat',scatter= False, x_ci= 'sd')

#%%

#plot the THI effect by parity and lactation stage for residuals!
actcual= data['total_activity']
estimated= np.exp(results.fittedvalues)
res= actcual - estimated

res= pd.Series.to_frame(res, name='residuals')

data_plot= pd.concat([res, data_input.loc[:,'THI_mean_56'], data_input.loc[:,'parity_group'], data_input.loc[:,'DIM_cat']],axis=1)

_ = sns.lmplot(x="THI_mean_56", y="residuals", data=data_plot, hue= 'parity_group', col='DIM_cat',scatter= False, x_ci= 'sd')

#%%
import math
# corr= (cov(x,y)/sqrt(var(x),var(y)))
corr= (-1.177)/ (math.sqrt(1611.259*3.048))

#%%
# plot a summer time in 2019 for residuals! 
actcual= data['total_activity']
estimated= np.exp(results.fittedvalues)
res= actcual - estimated

data_plot= pd.concat([data.loc[:,'datetime'],res],axis=1)

data_plot['datetime']= pd.to_datetime(data_plot['datetime'])
df = data_plot.set_index(pd.DatetimeIndex(data_plot['datetime']))

g= df.groupby(pd.Grouper(freq="d"))

g_mean= g.mean()
g_st= g.std()

summer_g_mean= g_mean['2019-06-01': '2019-08-31']  
summer_g_st= g_st['2019-06-01': '2019-08-31']  

#summer_g_mean= g_mean['2019-08-13': '2019-08-20']  
#summer_g_st= g_st['2019-08-13': '2019-08-20']  

ax = sns.lineplot(data= summer_g_mean.iloc[:,0]) #, err_style="unit_traces")

ax.errorbar(summer_g_mean.index, summer_g_mean.iloc[:,0], yerr=summer_g_st.iloc[:,0], fmt='-o') #fmt=None to plot bars only
ax.set_ylabel("Residuals", fontsize = 10)
ax.xaxis.set_major_locator(md.WeekdayLocator(byweekday = 1))
plt.setp(ax.xaxis.get_majorticklabels(), rotation = 90)

plt.show()
#%%
#weather info:
weather= pd.concat([data.loc[:,'datetime'], data.loc[:,'THI_mean']],axis=1)    
weather['datetime']= pd.to_datetime(weather['datetime'])
df_1 = weather.set_index(pd.DatetimeIndex(weather['datetime']))
g = df_1.groupby(pd.Grouper(freq="d"))
g_mean= g.mean()
summer_g_mean_w= g_mean['2019-06-01': '2019-09-30']    
    
#** 
#acitvity info

activity=pd.concat([data.loc[:,'datetime'], data.loc[:,'total_activity']],axis=1)  
activity['datetime']= pd.to_datetime(activity['datetime'])
df_2 = activity.set_index(pd.DatetimeIndex(activity['datetime']))
g= df_2.groupby(pd.Grouper(freq="d"))
g_mean= g.mean()
g_st= g.std()
# select the summer time:
#plot the activity per day mean and std!
summer_g_mean_A= g_mean['2019-06-01': '2019-09-30']    
# select the summer time:
summer_g_st_A= g_st['2019-06-01': '2019-09-30']  

#Predicted activity info:
    
p_activity=pd.concat([data.loc[:,'datetime'], results.fittedvalues],axis=1)  
p_activity['datetime']= pd.to_datetime(p_activity['datetime'])
df_3 = p_activity.set_index(pd.DatetimeIndex(p_activity['datetime']))
g= df_3.groupby(pd.Grouper(freq="d"))
g_mean= g.mean()
g_st= g.std()
# select the summer time:
#plot the activity per day mean and std!
summer_g_mean_P= g_mean['2019-06-01': '2019-09-30']    
# select the summer time:
summer_g_st_P= g_st['2019-06-01': '2019-09-30']  
    

# residuals_info

activity_residual= pd.concat([data.loc[:,'datetime'],results.resid],axis=1)

activity_residual['datetime']= pd.to_datetime(activity_residual['datetime'])
df_4 = activity_residual.set_index(pd.DatetimeIndex(activity_residual['datetime']))
g= df_4.groupby(pd.Grouper(freq="d"))

g_mean= g.mean()
g_st= g.std()

summer_g_mean_r= g_mean['2019-06-01': '2019-08-31']  
summer_g_st_r= g_st['2019-06-01': '2019-08-31']  




fig, axes = plt.subplots(4,1,figsize=(10, 15))
fig.suptitle('Summer time in 2019', fontsize=22,fontweight="bold")


sns.lineplot(ax= axes[0],data= summer_g_mean_w.iloc[:,0])  # change the columns number based on what we used (weather, weather+THI)

sns.lineplot(ax= axes[1],data= summer_g_mean_A.iloc[:,0]).errorbar(summer_g_mean_A.index, summer_g_mean_A.iloc[:,0], yerr=summer_g_st_A.iloc[:,0], fmt='-o') #fmt=None to plot bars only

sns.lineplot(ax= axes[2],data= summer_g_mean_P.iloc[:,0]).errorbar(summer_g_mean_P.index, summer_g_mean_P.iloc[:,0], yerr=summer_g_st_P.iloc[:,0], fmt='-o') #fmt=None to plot bars only

sns.lineplot(ax= axes[3],data= summer_g_mean_r.iloc[:,0]).errorbar(summer_g_mean_r.index, summer_g_mean_r.iloc[:,0], yerr=summer_g_st_r.iloc[:,0], fmt='-o') #fmt=None to plot bars only


plt.setp(axes[0].set(ylabel= 'THI'), fontsize=18)

plt.setp(axes[1].set(ylabel= 'Activity'), fontsize=18)

plt.setp(axes[2].set(ylabel= 'estimated_activity'), fontsize=18)

plt.setp(axes[3].set(ylabel= 'Residuals'), fontsize=18)

#plt.setp(axes[0].get_legend().get_texts(), fontsize=15) # for legend text

axes[3].xaxis.set_major_locator(md.WeekdayLocator(byweekday = 1))

plt.setp(axes[3].xaxis.get_majorticklabels(), rotation = 90, fontsize=18)


for ax in fig.get_axes():
    ax.label_outer()

plt.show()
#%

#%% To check what happen in the date of '2019-07-30': '2019-08-06'

actcual= data['total_activity']
estimated= np.exp(results.fittedvalues)
res= actcual - estimated



data_plot= pd.concat([data.loc[:,'datetime'],data.loc[:,'total_activity']],axis=1)
data_plot= pd.concat([data.loc[:,'datetime'], data.loc[:,'THI_mean']],axis=1)    
data_plot=pd.concat([data.loc[:,'datetime'], estimated],axis=1)  
data_plot= pd.concat([data.loc[:,'datetime'],res],axis=1)

#%%


data_plot['datetime']= pd.to_datetime(data_plot['datetime'])
df = data_plot.set_index(pd.DatetimeIndex(data_plot['datetime']))

g= df.groupby(pd.Grouper(freq="d"))

g_mean= g.mean()
g_st= g.std()

summer_g_mean= g_mean['2019-07-16': '2019-07-30']  #'2019-06-01': '2019-08-31'
summer_g_st= g_st['2019-07-16': '2019-07-30']   #'2019-06-01': '2019-08-31'

#summer_g_mean= g_mean['2019-08-13': '2019-08-20']  
#summer_g_st= g_st['2019-08-13': '2019-08-20']  

ax = sns.lineplot(data= summer_g_mean.iloc[:,0]) #, err_style="unit_traces")

ax.errorbar(summer_g_mean.index, summer_g_mean.iloc[:,0], yerr=summer_g_st.iloc[:,0], fmt='-o') #fmt=None to plot bars only
ax.set_ylabel("Activity", fontsize = 10)
ax.xaxis.set_major_locator(md.WeekdayLocator(byweekday = 1))
plt.setp(ax.xaxis.get_majorticklabels(), rotation = 90)

plt.show()