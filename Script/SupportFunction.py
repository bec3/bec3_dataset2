# -*- coding: utf-8 -*-
"""
Created on Fri Aug 26 15:55:27 2022

@author: tagha001
"""

#Add library
import numpy as np
import time
import cv2
#import imutils
#from imutils.video import FPS
#from imutils.video import VideoStream
from datetime import timedelta
import os
import xml.etree.ElementTree as ET
import copy
import pandas as pd
from skimage import io, transform
import matplotlib.pyplot as plt

from scipy.optimize import curve_fit

#%%

def F2_0_wood_model(x,p1,p2,p3):  
    
    """    
    Defines the (non-linear) Wood model as y = p1 * np.power(x,p2) * np.exp(-p3*x)       
    Within the F2_SelectionLactationHealthy script, this function is used in the F2_5_wm_2step_iter as a callable in the scipy.optimize.curve_fit function.
    Parameters    ----------    x : Array (dtype = float)        Contains the DIM values of each milking session within one (quarter or udder level) lactation   
    p1 : float        First parameter: represents the milk yield right after calving (the intercept).        Boundaries: [0 60 or 30]       
    Usual offset values: depend on quarter/udder level. Often taking the mean value of the y variable  
    p2 : float        Second parameter: represents the decreasing part of the lactation curve (after the production peak).     
    Boundaries:  [0 0.6]        Usual offset values: 0.2   
    p3 : float        Third parameter: represents the increasing part of the lactation curve (before the production peak).    
    Boundaries:  [0 0.01]        Usual offset values: 0.005    Returns    -------    y : Array (dtype = float)   
    Predicted standardised daily milk yields of each milking session within one (quarter or udder level) lactation.    
    """    
    x = x.astype(float)    
    y = p1 * np.power(x,p2) * np.exp((-p3*x))    
    return y
#%%
def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)